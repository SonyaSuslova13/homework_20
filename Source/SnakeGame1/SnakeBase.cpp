// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Food.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::DOWN;
	// UP - DOWN
}

// Called when the game starts or when spawned

void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	// GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, GetActorTransform());
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
}

void ASnakeBase::PLocation(FVector NewLocation)
{
	FTransform NewTransform(NewLocation);
	ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
	NewSnakeElem->SnakeOwner = this;
	int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
	if (ElemIndex == 0)
	{
		NewSnakeElem->SetFirstElementType();
	}
}


// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	if (SnakeElements.Num() == 0)
	{
		for (int i = 0; i < ElementsNum; ++i)
		{
			FVector NewLocation(SnakeElements.Num()*ElementSize, 0, 0);
			PLocation(NewLocation);
		}
	}

	else
	{
		ASnakeElementBase* TailSnakeElem = SnakeElements.Last();
		FVector PlacementOffset = TailSnakeElem->PlaceOffset;
		FVector TailLocation = TailSnakeElem->GetActorLocation();
		for (int i = 0; i < ElementsNum; ++i)

		
		{
			TailLocation += PlacementOffset;
			PLocation(TailLocation);

		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;

	}

	FVector PElemLocation, CurrentElemLocation;

	for (int i = 0; i < SnakeElements.Num(); i++)
	{
		if (i == 0)
		{
			PElemLocation = SnakeElements[i]->GetActorLocation();
			SnakeElements[i]->AddActorWorldOffset(MovementVector);
		}
		else
		{
			CurrentElemLocation = SnakeElements[i]->GetActorLocation();
			SnakeElements[i]->SetActorLocation(PElemLocation);
			if (i == SnakeElements.Num() - 1)
			{
				SnakeElements[i]->PlaceOffset = CurrentElemLocation - PElemLocation;
			}
			PElemLocation = CurrentElemLocation;
		}
	}
}


void ASnakeBase::SnakeElementOverlap(ASnakeElementBase * OverappendElement, AActor* Other)
{
	if (IsValid(OverappendElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverappendElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
};